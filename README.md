# 介绍
&emsp;&emsp;本项目是[《嵌入式LINUX应用开发完全手册》](https://baike.baidu.com/item/%E5%B5%8C%E5%85%A5%E5%BC%8FLINUX%E5%BA%94%E7%94%A8%E5%BC%80%E5%8F%91%E5%AE%8C%E5%85%A8%E6%89%8B%E5%86%8C)的第二版。</br>

&emsp;&emsp;第一版基于2440，在第二版我们试图支持更多的开发板，比如支持[100ASK_IMX6ULL](https://item.taobao.com/item.htm?spm=a1z10.1-c-s.w5003-22892812496.1.764a82acMcyA8t&id=610613585935&scene=taobao_shop)、firelfy公司的RK3288、[ROC-3399-PC](https://item.taobao.com/item.htm?spm=a1z10.5-c-s.w4002-18944745104.11.5a4c5e6d0W9mdn&id=601124209964)。因为芯片在不断发展，你用A芯片学习了，工作中很可能用另一款B芯片。[讲课时](https://www.100ask.net/detail/p_5e63533cd8f11_RgAmj3vM/8)，我们必须把通用的知识概括出来。如果只用一款芯片来讲课，我无法清晰地概括出通用知识。</br>

&emsp;&emsp;但是把所有开发板的资料放在一个PDF里，会有2000、3000页。这很难操作。</br>

&emsp;&emsp;我们的主打开发板是[100ASK_IMX6ULL](https://item.taobao.com/item.htm?spm=a1z10.1-c-s.w5003-22892812496.1.764a82acMcyA8t&id=610613585935&scene=taobao_shop)，所以本文档里会含有它的开发板使用手册，至于其他开发板的使用手册，可以在[百度网盘](http://download.100ask.net/)里[下载](http://download.100ask.net/)到。</br>

&emsp;&emsp;本书主要面向有MCU开发经验，希望从零开始学习Linux开发的嵌入式软件工程师及在校学生。[在线观看](https://book.100ask.net/)本书籍：[https://book.100ask.net/ ](https://book.100ask.net/ ) | [https://book.100ask.org/](https://book.100ask.org/) </br>

# 版权声明
&emsp;&emsp;百问科技©2020</br>

&emsp;&emsp;深圳[百问科技](https://www.100ask.net)有限公司版权所有，并保留对本项目及声明的一切权力。</br>

&emsp;&emsp;未得到本公司的书面许可，任何单位和个人不得以任何方式或形式对本项目内的任何部分进行复制、摘录、备份、修改、传播、翻译成其他语言、将其全部或部分用于商业用途</br>


# 如何参与项目
&emsp;&emsp;只需修改并提交 `./documentation ` 目录下的源文件即可，采纳后我们将会在下一个版本发布新的更改，并将你的修改记录加入贡献名单榜。


# 目录
## 第1篇 新学习路线、视频介绍、资料下载
&emsp;&emsp;从初学者角度出发，概括说明资料的布局，如何使用本文档进行学习，放在开篇作为一个指南，在学习过程中可参考这里获得学习方向指引。</br>

## 第2.1篇 100ASK_IMX6ULL开发板使用手册</br>
&emsp;&emsp;以100ASK_IMX6ULL开发板为例，手把手教您如何使用一款具体的开发板进行嵌入式Linux的学习的基础知识准备，这个过程同样可以适用于其他开发板(如:AM335x、RK3399等等)。</br>

## 第2.2篇 其他100ASK_IMX6ULL开发板快速上手
&emsp;&emsp;除了使用100ASK_6ULL开发板，您还可以选择其他多款的开发板进行学习，这里介绍了三克开发板的快速上手的过程(100ASK_AM335X、RK3288、RK3399)</br>

## 第3篇 Linux基本操作与开发工具使用
&emsp;&emsp;磨刀不误砍柴工，这里介绍Linux基本操作与开发工具使用，为后面的学习打下坚实的基础，后续学习更加得心应手、事半功倍。</br>

## 第4篇 嵌入式Linux应用开发基础知识
&emsp;&emsp;从深挖HelloWorld背后的原理出发，探索GCC编译、Makefile的内部原理及使用方法，让您知其然亦知其所以然，为后面的开发打下坚实的基础。之后深入学习文件IO，进程线程等等基础知识。</br>

## 第5篇 嵌入式Linux驱动开发基础知识
&emsp;&emsp;采取逐步深入学习的思路，从hello驱动开始、带您看懂原理图、理解驱动设计的思想，总线设备驱动模型、设备树、GPIO和Pinctrl子系统、异常与中断等等。</br>

## 第6篇 实战项目
&emsp;&emsp;将在2020年6月22号开始，第1个项目是电子产品的批量生产与测试，这涉及LCD、触摸屏、网络、进程/线程等诸多基础知识，这个项目就是用来把这些基础知识贯穿起来。</br>
第2个项目将在第1个项目的基础上使用摄像头实现物品识别，涉及人工智能的基本使用。</br>
更多其他项目将根据学员的建议来补充录制，现在未定，欢迎您也参与到我们的视频录制中来。</br>

## 第7篇 驱动大全
&emsp;&emsp;这篇永远不会完结，我们计划紧跟技术的发展，录制尽可能多的驱动程序，并且会比较深入地讲解。</br>

&emsp;&emsp;您要深入研究某个模块时，您的工作涉及某个模块时，可以参考驱动大全的某章节，把它当作字典来使用，这个字典会随着技术的发展不断更新。</br>

## 第8篇 调试技术
&emsp;&emsp;程序是三分写、七分调；会涉及gdb、strace、perf、kmemleak等调试技术，让您快速掌握实际工作、开发中用到的各种调试技术</br>

## 第9篇 专题
&emsp;&emsp;比如Uboot专题、内存管理专题，这些是比较高阶的课程，一般人用不到，有需要时再根据课程有针对性的进行学习即可。</br>

## 第10篇 裸机开发
&emsp;&emsp;近30个实验、100多个程序、讲解通俗易懂，包含了我们在工作和学习中常见的模块，通过大量的实验让您慢慢掌握ARM裸机开发方法。</br>

## 第11篇 常见问题
&emsp;&emsp;总结提炼学员在学习过程中遇到的常见问题，单独一篇最大程度避免大家重复踩坑，高效率无忧安心学习</br>

## 第12篇 附录(供参考，不重要)
&emsp;&emsp;总结工程师在工作过程中遇到的问题，软硬件经验、技巧。</br>

# 联系我们
- 百问网官方wiki：[http://wiki.100ask.org](http://wiki.100ask.org)
- 百问网官方论坛：[http://bbs.100ask.net](http://bbs.100ask.net)
- 百问网官网：[http://www.100ask.net](http://www.100ask.net)
- 微信公众号：百问科技
- CSDN：[韦东山](https://edu.csdn.net/lecturer/90)
- B站：[韦东山](https://space.bilibili.com/275908810?from=search&seid=10505231074028238949)
- 知乎：[韦东山嵌入式](https://www.zhihu.com/people/www.100ask/)
- 微博：[百问科技](https://weibo.com/888wds?topnav=1&wvr=6)
- 电子发烧友学院：[韦东山](http://t.elecfans.com/teacher/3.html)

&emsp;&emsp;微信公众号( baiwenkeji )  |  [视频教程在线学习平台](http://www.100ask.net)</br>
<center class="half">
   <img src="http://photos.100ask.net//ELADCMSecond/aboutus/followus.png" width="800"/>
</center>



